
# for debugging: https://yihui.org/tinytex/r/#debugging
install.packages("rmarkdown", dep = TRUE)
install.packages('tinytex')
tinytex::install_tinytex()
update.packages(ask = FALSE, checkBuilt = TRUE)
tinytex::tlmgr_update()
